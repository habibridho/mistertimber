<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'timber');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mI(?:$zW`OemLGaKN+r6pi$obiMDTyM6=.@T+PV=qr4/i=nqSue<Td@>jt~!EKl5');
define('SECURE_AUTH_KEY',  'LGc_H7]S,>OB2_&4Z /f1u=I2ApiEg9eFHi^9CAhB1NWzV<!1^@^Y$G03qWM:!~1');
define('LOGGED_IN_KEY',    '8_qW&>_xPV O5R&~|V#{9i_%u]t5!2?`F l:2V$QUlY`Nrb9-p0l2^Yy@q_~|R_w');
define('NONCE_KEY',        'sH>:jk@vq)Y+E;SAL{5|5Z7@[`{Tyf!>,u5Af{5Vin=5O]L{Eh^P$zs$}0;#`s8|');
define('AUTH_SALT',        ':i2tAr)i|, ~A7yI M78:e.FeLPoa),zS7{%jcEkj y1cu+ l)S|^uVljxuI,{/X');
define('SECURE_AUTH_SALT', '%.]w6:v5IlCkLYW|0d<yap)OCq9_<thH+%<2J6}$oO)1W!iG|(Gv>e T.MTofJ~|');
define('LOGGED_IN_SALT',   'R,~f[QE,|e4eJ5Tz(q:XCxX5-|?+P-DVr5n,mM!Wz-(G5zD;&!c51g{eb)]CW{Pu');
define('NONCE_SALT',       '%ZzKIG&m}su}TMm-<)8h/Y|]E9B+fR,zRoA5rLIwe31H5^9?--|d*k+`5*vZ5O[X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'timber_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
