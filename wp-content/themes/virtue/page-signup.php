<?php
/*
Template Name: Signup
*/
?>

	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
	
    <div id="content" class="container">
   		<div class="row">
     		<div class="main <?php echo kadence_main_class(); ?>" role="main">
				<?php get_template_part('templates/content', 'page'); ?>
			
				<?php 
					global $woocommerce;

					wc_print_notices();

					// do_action( 'woocommerce_before_checkout_form', $checkout );
					// If checkout registration is disabled and not logged in, the user cannot checkout
					// if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
					// 	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
					// 	return;
					// }

					// filter hook for include new pages inside the payment method
					$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() );
				?>
				<form name="checkout" method="post" class="checkout" action="<?php echo esc_url( $get_checkout_url ); ?>">
					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
					<p class="form-row form-row-first validate-required validate-email" id="billing_email_field">
						<label for="billing_email" class>
							Email Addres 
							<abbr class="required" title="required">*</abbr>
						</label>
						<input type="text" class="input-text" name="billing_email" id="billing_email" placeholder value>
					</p>
					<p class="form-row form-row-first validate-required" id="billing_first_name_field"><label for="billing_first_name" class="">First Name <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_first_name" id="billing_first_name" placeholder=""  value=""  />
					</p>
					<p class="form-row form-row-last validate-required" id="billing_last_name_field"><label for="billing_last_name" class="">Last Name <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_last_name" id="billing_last_name" placeholder=""  value=""  />
					</p>
					<div class="clear"></div>
					<?php if (!is_user_logged_in() && $checkout->enable_signup){ ?>
					<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>
						<?php if ( $checkout->enable_guest_checkout ) : ?>
							<p class="form-row form-row-wide create-account">
								<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
							</p>
						<?php endif; ?>
						<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>
						<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>
							<div class="create-account">
								<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>
									<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
					<?php endif; ?>
					<input type="password" class="input-text " name="account_password_2" id="account_password_2" placeholder="Confirm password">
				<?php } ?>
					<div class="clear"></div>
					<p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field"><label for="billing_address_1" class="">Address <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_address_1" id="billing_address_1" placeholder="Street address"  value=""  />
					</p>
					<p class="form-row form-row-wide address-field" id="billing_address_2_field"><input type="text" class="input-text " name="billing_address_2" id="billing_address_2" placeholder="Apartment, suite, unit etc. (optional)"  value=""  />
					</p>				
					<p class="form-row form-row-wide address-field validate-required" id="billing_city_field"><label for="billing_city" class="">Town / City <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_city" id="billing_city" placeholder="Town / City"  value=""  />
					</p>
					<p class="form-row form-row-first address-field validate-required validate-state" id="billing_state_field"><label for="billing_state" class="">Province <abbr class="required" title="required">*</abbr></label><select name="billing_state" id="billing_state" class="state_select"  placeholder="State / County">
						<option value="">Select a state&hellip;</option><option value="AC" >Daerah Istimewa Aceh</option><option value="SU" >Sumatera Utara</option><option value="SB" >Sumatera Barat</option><option value="RI" >Riau</option><option value="KR" >Kepulauan Riau</option><option value="JA" >Jambi</option><option value="SS" >Sumatera Selatan</option><option value="BB" >Bangka Belitung</option><option value="BE" >Bengkulu</option><option value="LA" >Lampung</option><option value="JK" >DKI Jakarta</option><option value="JB" >Jawa Barat</option><option value="BT" >Banten</option><option value="JT" >Jawa Tengah</option><option value="JI" >Jawa Timur</option><option value="YO" >Daerah Istimewa Yogyakarta</option><option value="BA" >Bali</option><option value="NB" >Nusa Tenggara Barat</option><option value="NT" >Nusa Tenggara Timur</option><option value="KB" >Kalimantan Barat</option><option value="KT" >Kalimantan Tengah</option><option value="KI" >Kalimantan Timur</option><option value="KS" >Kalimantan Selatan</option><option value="KU" >Kalimantan Utara</option><option value="SA" >Sulawesi Utara</option><option value="ST" >Sulawesi Tengah</option><option value="SG" >Sulawesi Tenggara</option><option value="SR" >Sulawesi Barat</option><option value="SN" >Sulawesi Selatan</option><option value="GO" >Gorontalo</option><option value="MA" >Maluku</option><option value="MU" >Maluku Utara</option><option value="PA" >Papua</option><option value="PB" >Papua Barat</option></select></p>				<p class="form-row form-row-last address-field validate-required validate-postcode" id="billing_postcode_field"><label for="billing_postcode" class="">Postcode / Zip <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_postcode" id="billing_postcode" placeholder="Postcode / Zip"  value=""  />
					</p>
					<div class="clear"></div>
					<p class="form-row form-row-last validate-required validate-phone" id="billing_phone_field"><label for="billing_phone" class="">Phone <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text " name="billing_phone" id="billing_phone" placeholder=""  value=""  />
					</p>
					<div class="clear"></div>
					<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Sign up" data-value="Sign up" />
				</form>
				<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
			</div><!-- /.main -->