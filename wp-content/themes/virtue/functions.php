<?php
/*-----------------------------------------------------------------------------------*/
/* Include Theme Functions */
/*-----------------------------------------------------------------------------------*/
load_theme_textdomain('virtue', get_template_directory() . '/languages');
require_once locate_template('/themeoptions/options/virtue_extension.php'); // Options framework extension
require_once locate_template('/themeoptions/framework.php');        // Options framework
require_once locate_template('/themeoptions/options.php');     		// Options framework
require_once locate_template('/lib/utils.php');           			// Utility functions
require_once locate_template('/lib/init.php');            			// Initial theme setup and constants
require_once locate_template('/lib/sidebar.php');         			// Sidebar class
require_once locate_template('/lib/config.php');          			// Configuration
require_once locate_template('/lib/cleanup.php');        			// Cleanup
require_once locate_template('/lib/nav.php');            			// Custom nav modifications
require_once locate_template('/lib/metaboxes.php');     			// Custom metaboxes
require_once locate_template('/lib/gallery_metabox.php');     		// Custom metaboxes
require_once locate_template('/lib/comments.php');        			// Custom comments modifications
require_once locate_template('/lib/shortcodes.php');      			// Shortcodes
require_once locate_template('/lib/gallery.php');      				// Gallery Shortcode
require_once locate_template('/lib/widgets.php');         			// Sidebars and widgets
require_once locate_template('/lib/aq_resizer.php');      			// Resize on the fly
require_once locate_template('/lib/scripts.php');        			// Scripts and stylesheets
require_once locate_template('/lib/custom.php');          			// Custom functions
require_once locate_template('/lib/admin_scripts.php');          	// Icon functions
require_once locate_template('/lib/authorbox.php');         		// Author box
require_once locate_template('/lib/custom-woocommerce.php'); 		// Woocommerce functions
require_once locate_template('/lib/virtuetoolkit-activate.php'); 	// Plugin Activation
require_once locate_template('/lib/custom-css.php'); 			    // Fontend Custom CSS
add_action('wp_enqueue_scripts', 'your_function_name');

function your_function_name() {
    // Enqueue the style
    wp_enqueue_style('my-script-slug',  get_stylesheet_directory_uri() . '/style.css');
    // Enqueue the script
    // wp_enqueue_script('my-script-slug',  get_stylesheet_directory_uri() . '/your_script.js');
}
//Untuk paginasi
function pagination($pages = '', $range = 0){  
    $showitems = ($range * 2)+1;  
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages){
			$pages = 1;
        }
    }   
    if(1 != $pages){
		echo "<div class=\"pagination\">";
		echo "<a href='".get_pagenum_link(1)."'>&laquo; First Post</a>";
        if($paged > 1 && $showitems < $pages){
			echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Newer Post</a>";
		}else{
			echo "<a>&lsaquo; Newer Post</a>";
		}
        /* nomor halaman 
		for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }
		*/
		echo "<span class='spasi'></span>";
		
        if ($paged < $pages && $showitems < $pages){
			echo "<a href=\"".get_pagenum_link($paged + 1)."\">Older Post &rsaquo;</a>";
		}else{
			echo "<a>Older Post &rsaquo;</a>";
		}
        echo "<a href='".get_pagenum_link($pages)."'>Last Post &raquo;</a>";
        echo "</div>\n";
    }
}

// Hook in
//add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
//Checkout jika user belum login
add_filter('woocommerce_checkout_must_be_logged_in_message','custom_checkout_login');
function custom_checkout_login(){
?>
		<form method="post" class="login">
			<?php do_action( 'woocommerce_login_form_start' ); ?>
			<table class="login_form_timber">
			<tr>
				<td width="150"><p>Email Address</p></td>
				<td><input type="text" class="input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" /></td>
			</tr>
			<tr>
				<td width="150"><p>Password</p></td>
				<td><input class="input-text" type="password" name="password" id="password" /></td>
			</tr>
			<tr>
				<td><p class="lost_password">
				<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>">Forgotten your password?</a>
				</p></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><?php do_action( 'woocommerce_login_form' ); ?>
				<p>
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<input type="submit" class="button" name="login" value="Login" /> 
				</p></td>
			</tr>
			</table>
			<?php do_action( 'woocommerce_login_form_end' ); ?>
		</form>
	<?php
	if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
	<div class="col-2">
		<h2>New here?</h2>
		<form method="post" class="register">
			<?php do_action( 'woocommerce_register_form_start' ); ?>
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
				<p class="form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>
			<?php endif; ?>
			<p class="form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
				<p class="form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="input-text" name="password" id="reg_password" />
				</p>
			<?php endif; ?>
			<!-- Spam Trap -->
			<div style="left:-999em; position:absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>
			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>
			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-register', 'register' ); ?>
				<input type="submit" class="button" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
			</p>
			<?php do_action( 'woocommerce_register_form_end' ); ?>
		</form>
	</div>
	<?php endif;
	return;
}
// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}