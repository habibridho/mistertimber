<?php
/**
 * Checkout billing information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div class="woocommerce-billing-fields">
	<?php if ( WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<?php 
		if ( ! is_user_logged_in() && $checkout->enable_signup ){
			echo "<h2>New here?</h2>";
		}else{
			echo "<h2>Shipping Information</h2>";
		}
		?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
 
		<table class="billing_form_timber">
			<tr>
				<td width="150"><p>Email Address</p></td>
				<td><?php woocommerce_form_field( "billing_email", $checkout->checkout_fields['billing']['billing_email'], $checkout->get_value("billing_email") ); ?></td>
			</tr>
			<tr>
				<td><p>First Name</p></td>
				<td><?php woocommerce_form_field( "billing_first_name", $checkout->checkout_fields['billing']['billing_first_name'], $checkout->get_value("billing_first_name") ); ?></td>
			</tr>
			<tr>
				<td><p>Last Name</p></td>
				<td><?php woocommerce_form_field( "billing_last_name", $checkout->checkout_fields['billing']['billing_last_name'], $checkout->get_value("billing_last_name") ); ?></td>
			</tr>
			<tr>
				<td><p>Address</p></td>
				<td><?php woocommerce_form_field( "billing_address_1", $checkout->checkout_fields['billing']['billing_address_1'], $checkout->get_value("billing_address_1") ); ?></td>
			</tr>
			<tr>
				<td><p></p></td>
				<td><?php woocommerce_form_field( "billing_address_2", $checkout->checkout_fields['billing']['billing_address_2'], $checkout->get_value("billing_address_2") ); ?></td>
			</tr>
			<tr>
				<td><p></p></td>
				<td><?php woocommerce_form_field( "billing_city", $checkout->checkout_fields['billing']['billing_city'], $checkout->get_value("billing_city") ); ?></td>
			</tr>
			<?php if (!is_user_logged_in() && $checkout->enable_signup){ ?>
			<tr>
				<td><p>Password</p></td>
				<td>
				<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>
					<?php if ( $checkout->enable_guest_checkout ) : ?>
						<p class="form-row form-row-wide create-account">
							<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
						</p>
					<?php endif; ?>
					<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>
					<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>
						<div class="create-account">
							<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>
								<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
				<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td><p>Confirm Password</p></td>
				<td><input type="password" class="input-text " name="account_password_2" id="account_password_2"></td>
			</tr>
			<?php } ?>
			<tr>
				<td><p>Province<br><br>Postal Code</p></td>
				<td><?php woocommerce_form_field( "billing_state", $checkout->checkout_fields['billing']['billing_state'], $checkout->get_value("billing_state") ); ?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php woocommerce_form_field( "billing_postcode", $checkout->checkout_fields['billing']['billing_postcode'], $checkout->get_value("billing_postcode") ); ?></td>
			</tr>
			<tr>
				<td><p>Mobile No</p></td>
				<td><?php woocommerce_form_field( "billing_phone", $checkout->checkout_fields['billing']['billing_phone'], $checkout->get_value("billing_phone") ); ?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php woocommerce_form_field( "billing_country", $checkout->checkout_fields['billing']['billing_country'], $checkout->get_value("billing_country") ); ?></td>
			</tr>
		</table>
			
	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>