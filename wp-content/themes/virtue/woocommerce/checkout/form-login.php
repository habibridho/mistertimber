<?php
/**
 * Checkout login form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) return;

$info_message  = apply_filters( 'woocommerce_checkout_login_message', __( 'Already a member?', 'woocommerce' ) );
$info_message .= ' <a href="#" class="showlogin">' . __( 'Click here to login', 'woocommerce' ) . '</a>';
wc_print_notice( $info_message, 'notice' );

?>
<form method="post" class="login">
	<?php do_action( 'woocommerce_login_form_start' ); ?>
	<table class="login_form_timber">
	<tr>
		<td width="150"><p>Email Address</p></td>
		<td><input type="text" class="input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" /></td>
	</tr>
	<tr>
		<td width="150"><p>Password</p></td>
		<td><input class="input-text" type="password" name="password" id="password" /></td>
	</tr>
	<tr>
		<td><p class="lost_password">
		<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>">Forgotten your password?</a>
		</p></td>
	</tr>
	<tr>
		<td></td>
		<td align="right"><?php do_action( 'woocommerce_login_form' ); ?>
		<p>
		<?php wp_nonce_field( 'woocommerce-login' ); ?>
		<input type="submit" class="button" name="login" value="Login" /> 
		</p></td>
	</tr>
	</table>
	<?php do_action( 'woocommerce_login_form_end' ); ?>
</form>
<?php
/*	
	woocommerce_login_form(
		array(
			'redirect' => get_permalink( wc_get_page_id( 'checkout' ) ),
			'hidden'   => true
		)
	);
*/
?>