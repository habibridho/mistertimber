<?php 
/**
 * WooCommerce JNE Shipping ( Table )
 *
 * Untuk menampilkan halaman admin
 *
 * @author	ItsMeFurZy
 * @category	Core
 * @package	WooCommerce JNE Shipping
 */
	$option_save = get_option($this->jne_save);
?>
	<tr valign="top">			  	
		<th scope="row" class="titledesc"><?php _e('Pilih Tampilan City', 'woojne') ?></th>
		<td>
			<input type="radio" name="jneview" value="form-row-wide" id="full" <?php checked( $option_save['tampilan'], 'form-row-wide'); ?> <?php if(empty($option_save)) echo 'checked="checked"'; ?>> <label for="full">Full Width</label> &nbsp;
			<input type="radio" name="jneview" value="form-row-first" id="left" <?php checked( $option_save['tampilan'], 'form-row-first'); ?>> <label for="left">Left</label> &nbsp;
			<input type="radio" name="jneview" value="form-row-last" id="right" <?php checked( $option_save['tampilan'], 'form-row-last'); ?>> <label for="right">Right</label> &nbsp;
		</td>
	</tr>
	<tr valign="top">			  	
		<th scope="row" class="titledesc"><?php _e('Aktifkan Tarif JNE', 'woojne') ?></th>
		<td>
			<input type="checkbox" name="oke" value="yes" id="oke" <?php checked( $option_save['oke'], 'yes'); ?> <?php if(empty($option_save)) echo 'checked="checked"'; ?>> <label for="oke">OKE</label> &nbsp;
			<input type="checkbox" name="reg" value="yes" id="reg" <?php checked( $option_save['reg'], 'yes'); ?> <?php if(empty($option_save)) echo 'checked="checked"'; ?>> <label for="reg">Reguler</label> &nbsp;
			<input type="checkbox" name="yes" value="yes" id="yes" <?php checked( $option_save['yes'], 'yes'); ?> <?php if(empty($option_save)) echo 'checked="checked"'; ?>> <label for="yes">Express</label> &nbsp;
		</td>
	</tr>
	<tr valign="top">			  	
		<th scope="row" class="titledesc"><?php _e('Impor Kota', 'woojne') ?></th>
		<td>
			<p>
				<input type="file" name="woocommerce_jne_import_city" id="woocommerce_jne_import_city" style="min-width:393px;" />	      
			</p>
			<textarea name="woocommerce_jne_import_city_txt" id="woocommerce_jne_import_city_txt" style="min-width:600px;min-height:250px;"><?php
				$count == 0;			    
				$shipping_cities = $this->get_jne();
				if(count($shipping_cities) > 0 && !empty($shipping_cities)){
					foreach($shipping_cities as $shipping_city) {
						$output .= $shipping_city['provinsi'].',';
						$output .= $shipping_city['city'];
						foreach($shipping_city['price'] as $price) {
							$output .= ','.$price;
						}
						$output .= "\n";
					}
					echo $output;
				}?></textarea>
			<br/><i>Format: <b>Provinsi, Nama Kota, OKE, REG, YES</b></i>
		</td>
	</tr>
	<tr valign="top" class="controlbutton">
		<th scope="row" class="titledesc"></th>		
		<td>
			<a href="<?php echo jne_data_url ?>" class="button-primary" title="Dengan fitur ini memungkinkan Anda membackup data pengiriman Anda, setelah Anda buka tekan CTRL + Save. Info lebih lanjut harap lihat di See Documentation">Backup Data</a>
			<input name="jne_delete" class="button-primary" type="submit" id="jne_delete" value="Hapus Semua Data" title="Ketika Anda mengklik dan mengkonfirmasikannya Plugin ini akan menghapus semua data tarif">
		</td>     
	</tr>
	<tr valign="top">
		<td></td>
		<td>Developed By <a href="http://www.agenwebsite.com/" target="_blank" title="AgenWebsite.com - Web Services and Development">AgenWebsite</a> | <a href="http://www.agenwebsite.com/products/woocommerce-jne-shipping" target="_blank">See Documentation</a></td>      
	</tr> 

</table><!--/.form-table-->
<script type="text/javascript">
	jQuery(function() {
		jQuery('.controlbutton').on( 'click', '#jne_delete', function(){
			var answer = confirm("Hapus semua data jne?")
			if (!answer) {
				return false;
			}
		});

	});
</script>