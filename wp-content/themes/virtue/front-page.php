			<?php  global $virtue; ?>
			<?php if(!empty($virtue['mobile_switch'])) { 
				$mobile_slider = $virtue['mobile_switch'];
			} else {
				$mobile_slider = '';
			}
			if(!empty($virtue['choose_slider'])) {
				$slider = $virtue['choose_slider'];
			} else {
				$slider = 'mock_flex';
			}
			if(detect_mobile() && $mobile_slider == '1') {
		 		$slider = $virtue['choose_mobile_slider'];
					 if ($slider == "flex") {
					get_template_part('templates/mobile_home/mobileflex', 'slider');
				}
				else if ($slider == "video") {
					get_template_part('templates/mobile_home/mobilevideo', 'block');
				} 
	} else { ?>
    		<?php if ($slider == "flex") {
					get_template_part('templates/home/flex', 'slider');
				}
				else if ($slider == "thumbs") {
					get_template_part('templates/home/thumb', 'slider');
				}
				else if ($slider == "latest") {
					get_template_part('templates/home/latest', 'slider');
				}
				else if ($slider == "carousel") {
					get_template_part('templates/home/carousel', 'slider');
				}
				else if ($slider == "video") {
					get_template_part('templates/home/video', 'block');
				}
				else if ($slider == "mock_flex") {
					get_template_part('templates/home/mock', 'flex');
				}
}

if(isset($virtue['homepage_layout']['enabled'])){
		$i = 0;
		$show_pagetitle = false;
		foreach ($virtue['homepage_layout']['enabled'] as $key=>$value) {
			if($key == "block_one") {
				$show_pagetitle = true;
			}
			$i++;
			if($i==2) break;
		}
	} ?>

	<?php if($show_pagetitle == true) {
		?><div id="homeheader" class="welcomeclass">
								<div class="container">
									<div class="row">
										<div class="col-md-6">
											<img class="kanan" src="<?= get_template_directory_uri(); ?>/assets/img/mascot.png">
										</div>
										<div class="col-md-6">
											<img src="<?= get_template_directory_uri(); ?>/assets/img/bubble.png">
										</div>
									</div>
									<div class="row section">
										<h1>How it Works</h1>
										<div class="clear"></div>
										<div class="clear"></div>
										<div class="row">
											<div class="col-md-4">
												<img src="<?= get_template_directory_uri(); ?>/assets/img/works1.png ?>"><br>
												<div class="clear"></div>
											</div>
											<div class="col-md-4">
												<img src="<?= get_template_directory_uri(); ?>/assets/img/works2.png ?>"><br>
												<div class="clear"></div>
											</div>
											<div class="col-md-4">
												<img src="<?= get_template_directory_uri(); ?>/assets/img/works3.png ?>"><br>
												<div class="clear"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<p class="description">Beritahukan kami varian Mister Timber yang kamu sukai</p>
											</div>
											<div class="col-md-4">
												<p class="description">Isi formulir order online dan bayar melalui bank</p>
											</div>
											<div class="col-md-4">
												<p class="description">Kemudian kami akan mengirimkan ke rumah atau tempat kerja kamu</p>
											</div>
										</div>
									</div>
									<div class="row section">
										<h1>Our Products</h1>
										<div class="clear"></div>
										<div class="col-md-6 col-md-offset-3">
											<p>MisterTimber® dibuat dari perpaduan bahan baku oat dan almond pilihan dan dipanggang hingga mendapatkan rasa yang sempurna serta memiliki kandungan yang bermanfaat bagi tubuh. Mister Timber hadir dalam 4 varian rasa yang menarik, yaitu Chocolate, Cheese, Raisin Strawberry dan Raisin Banana.</p>
										</div>
										<div class="clear"></div>
										<div class="clear"></div>
									</div>
									<!-- <?php get_template_part('templates/page', 'header'); ?> -->
								</div>
				
							</div><!--titleclass-->
								<div class="container-fluid products">
									<div class="row">
										<img src="<?= get_template_directory_uri(); ?>/assets/img/products.png">
									</div>
									<div class="row">
										<div class="clear100"></div>
										<div class="clear100"></div>
										<div class="col-md-6 col-md-offset-3">
											<div class="dash">
												<p>Low Calories, Low Gluten, Good Source of Fiber, No Preservatives, and Better because it’s baked!</p>
											</div>
										</div>
									</div>
									<div class="row section">
										<h1>Featured Products!</h1>
										<div class="clear"></div>
										<div class="clear"></div>
										<div class="col-md-8 col-md-offset-2">
											<div class="row">
												<div class="col-md-6">
													<img src="<?= get_template_directory_uri(); ?>/assets/img/featured1.png">
												</div>
												<div class="col-md-6">
													<img src="<?= get_template_directory_uri(); ?>/assets/img/featured2.png">
												</div>
											</div>
											<div class="clear"></div>
											<div class="row">
												<div class="col-md-6">
													<img src="<?= get_template_directory_uri(); ?>/assets/img/featured3.png">
												</div>
												<div class="col-md-6">
													<img src="<?= get_template_directory_uri(); ?>/assets/img/featured4.png">
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- <div class="container"> -->
									<div class="row section">
										<h1>Stay Updated</h1>
									</div>
										<div class="clear"></div>
										<div class="clear"></div>
										<img class="stayupdated" src="<?= get_template_directory_uri(); ?>/assets/img/stayupdate.png">
									<!-- </div> -->
								<!-- </div> -->
	<?php } ?>