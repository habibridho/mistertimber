<?php
/*
Template Name: About
*/
?>

	<div id="pageheader" class="titleclass">
		<div class="container">
			<div class="page-header">
				<div class="row"><div class="col-md-offset-1">
			  <h1>Our Story</h1>
			   	</div></div>
			</div>
		</div><!--container-->
	</div><!--titleclass-->
	
    <div id="content" class="container">
   		<div class="row">
   			<div class="row"> <!-- custom row -->
   				<div class="main col-md-10 col-md-offset-1 <?php echo kadence_main_class(); ?>" role="main">
					<?php get_template_part('templates/content', 'page'); ?>
				</div><!-- /.main -->
   			</div><!-- custom row -->

   			<div class="clear"></div>
   			<div class="clear"></div>

   			<div id="pageheader" class="titleclass">
				<div class="container">
					<div class="page-header">
						<div class="row"><div class="col-md-offset-1">
					  <h1>Meet our Team</h1>
					   	</div></div>
					</div>
				</div><!--container-->
			</div><!--titleclass-->

			<div class="row">
				<?php do_action( 'woothemes_our_team' ); ?>
			</div>
   			